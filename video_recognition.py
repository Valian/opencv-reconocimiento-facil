import cv2
import numpy as np
import pickle

cap = cv2.VideoCapture(0)
frontalFace = cv2.CascadeClassifier('cascades/haarcascade_frontalface_alt2.xml')
smile_cascade = cv2.CascadeClassifier('cascades/haarcascade_smile.xml')

faceClassif = frontalFace

labels = {}
recognizer = cv2.face.LBPHFaceRecognizer_create()

with open("pickles/face-labels.pickle", 'rb') as f:
    labels = pickle.load(f)
    labels = {v: k for k, v in labels.items()}

recognizer.read('recognizers/face-trainner.yml')
while True:
    ret, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    faces = faceClassif.detectMultiScale(gray, 1.3, 5)

    for (x, y, w, h) in faces:
        cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)
        img_gray = gray[y:y + h, x:x + w]
        img_color = frame[y:y + h, x:x + w]

        id_, conf = recognizer.predict(img_gray)
        if 45 <= conf <= 85:
            font = cv2.FONT_HERSHEY_SIMPLEX
            name = labels[id_]
            color = (255, 255, 255)
            stroke = 2
            cv2.putText(frame, name.replace("-", " "), (x, y), font, 1, color, stroke, cv2.LINE_AA)

            # color = (255, 255, 255)
            # stroke = 2
            # end_cord_x = x + w
            # end_cord_y = y + h
            # cv2.rectangle(frame, (x, y), (end_cord_x, end_cord_y), color, stroke)
            # smile = smile_cascade.detectMultiScale(img_gray)
            # for (ex, ey, ew, eh) in smile:
            #    cv2.rectangle(img_color, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)

        img_item = 'yo.png'
        cv2.imwrite(img_item, img_color)

    cv2.imshow('frame', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
cap.release()
cv2.destroyAllWindows()
